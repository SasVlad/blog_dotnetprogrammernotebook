﻿using Blog_DotNetProgrammerNotebook.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Blog_DotNetProgrammerNotebook.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> TestIndex()
        {    
            return View();
        }
    }
}
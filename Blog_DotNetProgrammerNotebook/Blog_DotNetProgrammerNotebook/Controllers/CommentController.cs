﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog_DotNetProgrammerNotebook.Controllers
{
    public class CommentController : Controller
    {
        CommentService _commentService = new CommentService();
        // GET: Comment
        [HttpGet]
        public ActionResult GetAllComments()
        {
            return View(_commentService.GetAllCommentsAsync());
        }
        [HttpGet]
        public ActionResult GetAllCommentsByPostId(int postId)
        {
            //Comment && User
            return View(_commentService.GetAllCommentsByPostIdAsync(postId));
        }

        // ?? 
        [HttpGet]
        public ActionResult GetCommentById(int commentid)
        {
            var result = _commentService.GetCommentByIdAsync(commentid);

            return View();
        }
        [HttpGet]
        public ActionResult CreateComment()
        {
            return View();
        }
        [HttpPost]
        public void CreateComment(Comment comment)
        {
            comment.DateRecord = DateTime.Now;
            var result = _commentService.CreateCommentAsync(comment);

            if (result != null)
            {
            
            }

        }

        [HttpGet]
        public ActionResult DeleteCommentById(int commentid)
        {
            _commentService.DeleteCommentByIdAsync(commentid);
            return RedirectToAction("GetAllComments");
        }
        [HttpPost]
        public ActionResult UpdateComment(Comment comment)
        {
            var result = _commentService.UpdateCommentAsync(comment);
            if (result != null)
            {
                return RedirectToAction("GetAllComments");
            }
            return View();
        }
    }
}
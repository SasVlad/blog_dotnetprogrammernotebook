﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Blog_DotNetProgrammerNotebook.Controllers
{
    public class PostController : Controller
    {
        PostService _postService = new PostService();
        CommentService _commentService = new CommentService();
        UserService _userService = new UserService();
        // GET: Post
        [HttpGet]
        public async Task<JsonResult> GetAllPostsJson()
        {
            return Json(await _postService.GetAllPostsAsync(),JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetAllPosts()
        {
            return View();
        }
        [HttpGet]
        public async Task<JsonResult> GetAllPostsByCategoryIdJson(int categoryId)
        {
            var res = await _postService.GetAllPostsByCategoryIdAsync(categoryId);
            return Json( await _postService.GetAllPostsByCategoryIdAsync(categoryId), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetAllPostsByAuthorId(int authorId)
        {
            return View(_postService.GetAllPostsByAuthorIdAsync(authorId));
        }
        [HttpGet]
        public ActionResult GetPostById()
        {
            return View();
        }
        [HttpGet]
        public async Task<JsonResult> GetPostByIdJson(int postid)
        {
            var result = await _postService.GetPostByIdAsync(postid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult CreatePost()
        { 
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> CreatePost(Post post)
        {          
            post.DatePublished = DateTime.Now;
            var result = await _postService.CreatePostAsync(post);

            if (result != null)
            {
                return Json(result,JsonRequestBehavior.AllowGet);
            }
            return Json(new Post(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult DeletePostById(int postid)
        {
            _postService.DeletePostByIdAsync(postid);
            return RedirectToAction("GetAllPosts");
        }
        [HttpPost]
        public ActionResult UpdatePost(Post post)
        {
            var result = _postService.UpdatePostAsync(post);
            if (result != null)
            {
                return RedirectToAction("GetAllPosts");
            }
            return View();
        }
    }
}
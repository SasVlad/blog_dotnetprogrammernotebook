﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Blog_DotNetProgrammerNotebook.Controllers
{
    public class UserController : Controller
    {
        UserService _userService = new UserService();
        // GET: User
        [HttpGet]
        public ActionResult GetAllUsers()
        {
            return View(_userService.GetAllUsersAsync());
        }
        // ?? 
        [HttpGet]
        public async Task<JsonResult> GetUserById(int userId)
        {
            var result = await _userService.GetUserByIdAsync(userId);

            return Json(result,JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult AuthorizeUser()
        {
            return View();
        }
        [HttpGet]
        public ActionResult RestorePasswordCheckEmailUser()
        {
            return View();
        }
        [HttpGet]
        public ActionResult RestorePasswordCheckKeyQuestionUser()
        {
            return View();
        }
        [HttpGet]
        public ActionResult RestorePasswordUser()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> CheckAuthorizeUserJson(User user)
        {
            var result =await _userService.CheckAuthorizeUserAsync(user.Email, user.Password);

            if (result != null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(new User());
        }
        [HttpPost]
        public async Task<JsonResult> CheckUserKeyAnswerJson(User user)
        {
            var result = await _userService.CheckUserKeyAnswer(user.KeyAnswer, user.Id);

            if (result != null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(new User());
        }
        [HttpGet]
        public ActionResult CreateUser()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> CreateUser(User user)
        {
            var result = await _userService.CreateUserAsync(user);

            if (result != null)
            {
                return Json(result,JsonRequestBehavior.AllowGet);
            }
            return Json(new User());


        }
        [HttpGet]
        public ActionResult DeleteUserById(int userid)
        {
            _userService.DeleteUserByIdAsync(userid);
            return RedirectToAction("GetAllUsers");
        }
        [HttpPost]
        public async Task<JsonResult> UpdateUser(User user)
        {
            var result = await _userService.UpdateUserAsync(user);
            if (result != null)
            {
            }
            return Json(result,JsonRequestBehavior.AllowGet);
        }
    }
}
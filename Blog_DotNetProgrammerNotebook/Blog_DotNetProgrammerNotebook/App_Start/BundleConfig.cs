﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Blog_DotNetProgrammerNotebook
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/bootstrap-theme.min.css"));
            bundles.Add(new ScriptBundle("~/AngularJS").Include("~/Scripts/angular.js",
                "~/Scripts/angular-route.js", "~/Scripts/angular-cookies.js"));
            bundles.Add(new ScriptBundle("~/app").Include("~/App/blogModule.js",
                "~/App/blogRoutes.js"));
            bundles.Add(new ScriptBundle("~/app/controllers")
                .Include("~/App/controllers/categoryController.js",
                    "~/App/controllers/createCategoryController.js",
                    "~/App/controllers/createNewPostController.js",
                    "~/App/controllers/postByIdController.js",
                    "~/App/controllers/postController.js",
                    "~/App/controllers/postsByCategoryIdController.js",
                    "~/App/controllers/authorizeUserController.js",
                    "~/App/controllers/restorePasswordCheckEmailController.js",
                    "~/App/controllers/restorePasswordCheckKeyQuestionController.js",
                    "~/App/controllers/restorePasswordController.js",
                    "~/App/controllers/authorizeController.js",
                    "~/App/controllers/createUserController.js"));
            bundles.Add(new ScriptBundle("~/bundles/clientfeaturesscripts")
                .Include("~/Scripts/jquery-3.1.1.min.js",
                    "~/Scripts/bootstrap.min.js",
                    "~/Scripts/modernizr-2.6.2.js"));
        }
    }
}
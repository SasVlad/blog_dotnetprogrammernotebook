﻿using Blog_DotNetProgrammerNotebook.EF;
using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Repositories
{
    public class PostRepository
    {
        public async Task<List<Post>> GetAllPostsAsync()
        {
            var listPosts = new List<Post>();
            using (var context = new BlogContext())
            {
                listPosts = await context.Posts
                    .Include(x => x.Author).Include(x => x.Category).Include(x => x.Comments).ToListAsync();
            }
            return listPosts;
        }
        public async Task<List<Post>> GetAllPostsByCategoryIdAsync(int categoryId)
        {
            var listPosts = new List<Post>();
            using (var context = new BlogContext())
            {
                listPosts = await context.Posts.Where(x=>x.CategoryId==categoryId)
                    .Include(x => x.Author).Include(x => x.Category).ToListAsync();
            }
            return listPosts;
        }
        public async Task<List<Post>> GetAllPostsByAuthorIdAsync(int authorId)
        {
            var listPosts = new List<Post>();
            using (var context = new BlogContext())
            {
                listPosts = await context.Posts.Where(x => x.AuthorId == authorId)
                .Include(x=>x.Author).Include(x=>x.Category).ToListAsync();
            }
            return listPosts;
        }
        public async Task<Post> GetPostByIdAsync(int Id)
        {
            var post = new Post();
            using (var context = new BlogContext())
            {
                post = await context.Posts.FirstOrDefaultAsync(x => x.Id == Id);
                await context.Entry(post).Reference("Author").LoadAsync();
                await context.Entry(post).Reference("Category").LoadAsync();
                await context.Entry(post).Collection("Comments").Query().Include("User").LoadAsync();
            }
            return post;
        }

        public async Task<Post> CreatePostAsync(Post post)
        {
            var currentPost = new Post();
            using (var context = new BlogContext())
            {
                currentPost = await context.Posts.FirstOrDefaultAsync(x => x.Article == post.Article);

                if (currentPost == null)
                {
                    context.Posts.Add(post);
                    await context.SaveChangesAsync();
                    currentPost = post;
                }
            }
            return currentPost;
        }

        public async void DeletePostByIdAsync(int id)
        {
            var post = new Post();
            using (var context = new BlogContext())
            {
                post = await context.Posts.FirstOrDefaultAsync(x => x.Id == id);

                if (post != null)
                {
                    context.Posts.Remove(post);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<Post> UpdatePostAsync(Post post)
        {
            var currentPost = new Post();
            using (var context = new BlogContext())
            {
                currentPost = await context.Posts.FirstOrDefaultAsync(x => x.Id == post.Id);

                if (currentPost != null)
                {
                    context.Posts.AddOrUpdate(post);
                   await context.SaveChangesAsync();
                    currentPost = post;
                }
            }
            return currentPost;
        }
    }
}
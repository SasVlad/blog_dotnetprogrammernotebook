﻿using Blog_DotNetProgrammerNotebook.EF;
using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace Blog_DotNetProgrammerNotebook.Repositories
{
    public class UserRepository
    {
        //public UserRepository()
        //{
        //    using (var ctx = new BlogContext())
        //    {
        //        using (var writer = new XmlTextWriter($@"{AppDomain.CurrentDomain.BaseDirectory}\BlogEDM.edmx", Encoding.Default))
        //        {
        //            EdmxWriter.WriteEdmx(ctx, writer);
        //        }
        //    }
        //}
        public async Task<List<User>> GetAllUsersAsync()
        {
            var listUsers = new List<User>();
            using (var context = new BlogContext())
            {
                listUsers = await context.Users.ToListAsync();
            }
            return listUsers;
        }
        public async Task<User> GetUserByIdAsync(int Id)
        {
            var user = new User();
            using (var context = new BlogContext())
            {
                user = await context.Users.FirstOrDefaultAsync(x => x.Id == Id);
            }
            return user;
        }
        public async Task<User> CheckAuthorizeUserAsync(string email, string password="")
        {
            var user = new User();
            using (var context = new BlogContext())
            {
                if (!string.IsNullOrEmpty(password))
                {
                    user = await context.Users.FirstOrDefaultAsync(x => x.Email == email && x.Password == password);
                }

                if (string.IsNullOrEmpty(password))
                {
                    user = await context.Users.FirstOrDefaultAsync(x => x.Email == email);
                }

                if (user != null)
                {
                    return user;
                }
            }
            return null;
        }

        public async Task<User> CheckUserKeyAnswer(string keyAnswer, int userId)
        {
            var currentUser = new User();
            using (var context = new BlogContext())
            {
                currentUser = await context.Users.FirstOrDefaultAsync(x => x.Id == userId&&x.KeyAnswer== keyAnswer);
            }
            return currentUser;        
        }

        public async Task<User> CreateUserAsync(User user)
        {
            var currentUser = new User();
            using (var context = new BlogContext())
            {
                currentUser = await context.Users.FirstOrDefaultAsync(x => x.Email == user.Email);

                if (currentUser == null)
                {
                    context.Users.Add(user);
                    await context.SaveChangesAsync();
                    currentUser = user;
                }
            }
            return currentUser;
        }

        public async void DeleteUserByIdAsync(int id)
        {
            var user = new User();
            using (var context = new BlogContext())
            {
                user = await context.Users.FirstOrDefaultAsync(x => x.Id == id);

                if (user != null)
                {
                    context.Users.Remove(user);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<User> UpdateUserAsync(User user)
        {
            var currentUser = new User();
            using (var context = new BlogContext())
            {
                currentUser = await context.Users.FirstOrDefaultAsync(x => x.Id == user.Id);

                if (currentUser != null)
                {
                    context.Users.AddOrUpdate(user);
                    await context.SaveChangesAsync();
                    currentUser = user;
                }
            }
            return currentUser;
        }
    }
}
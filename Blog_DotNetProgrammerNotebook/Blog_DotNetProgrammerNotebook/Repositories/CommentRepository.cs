﻿using Blog_DotNetProgrammerNotebook.EF;
using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Repositories
{
    public class CommentRepository
    {
        public async Task<List<Comment>> GetAllCommentsAsync()
        {
            var listComments = new List<Comment>();
            using (var context = new BlogContext())
            {
                listComments= await context.Comments.Include(x => x.User).ToListAsync();
            }
            return listComments;
        }
        public async Task<List<Comment>> GetAllCommentsByPostIdAsync(int postId)
        {
            var listComments = new List<Comment>();
            using (var context = new BlogContext())
            {
                listComments = await context.Comments.Include(x => x.User).ToListAsync();
            }
            return listComments;
        }
        public async Task<Comment> GetCommentByIdAsync(int Id)
        {
            var comment = new Comment();
            using (var context = new BlogContext())
            {
                comment = await context.Comments.FirstOrDefaultAsync(x => x.Id == Id);
                await context.Entry(comment).Reference("User").LoadAsync();
            }
            return comment;
        }

        public async Task<Comment> CreateCommentAsync(Comment comment)
        {
            var currentComment = new Comment();
            using (var context = new BlogContext())
            {
                currentComment = comment;

                if (currentComment != null)
                {
                    context.Comments.Add(comment);
                    await context.SaveChangesAsync();
                    currentComment = comment;
                }
            }
            return currentComment;
        }

        public async void DeleteCommentByIdAsync(int id)
        {
            var comment = new Comment();
            using (var context = new BlogContext())
            {
                comment = await context.Comments.FirstOrDefaultAsync(x => x.Id == id);

                if (comment != null)
                {
                    context.Comments.Remove(comment);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<Comment> UpdateCommentAsync(Comment comment)
        {
            var currentComment = new Comment();
            using (var context = new BlogContext())
            {
                currentComment = await context.Comments.FirstOrDefaultAsync(x => x.Id == comment.Id);

                if (currentComment != null)
                {
                    context.Comments.AddOrUpdate(comment);
                    await context.SaveChangesAsync();
                    currentComment = comment;
                }
            }
            return currentComment;
        }
    }
}
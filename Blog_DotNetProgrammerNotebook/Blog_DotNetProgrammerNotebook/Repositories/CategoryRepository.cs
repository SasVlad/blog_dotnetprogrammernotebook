﻿using Blog_DotNetProgrammerNotebook.EF;
using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace Blog_DotNetProgrammerNotebook.Repositories
{
    public class CategoryRepository
    {
        public CategoryRepository()
        {
           // using (var ctx = new BlogContext())
           // {
           //     using (var writer = new XmlTextWriter($@"{AppDomain.CurrentDomain.BaseDirectory}\BlogEDM.edmx", Encoding.Default))
           //     {
           //         EdmxWriter.WriteEdmx(ctx, writer);
           //     }
           //}
        
        }
        public async Task<List<Category>> GetAllCategoriesAsync()
        {
            using (var context = new BlogContext())
            {
                return await context.Categories.ToListAsync();
            }
        }
        public async Task<Category> GetCategoryByIdAsync(int id)
        {
            var category = new Category();
            using (var context = new BlogContext())
            {
                category = await context.Categories.FirstOrDefaultAsync(x=>x.Id==id);
            }
            return category;           
        }

        public async Task<Category> CreateCategoryAsync(Category category)
        {
           var currentCategory = new Category();
           using (var context = new BlogContext())
           {
                currentCategory = await context.Categories.FirstOrDefaultAsync(x=>x.Name==category.Name);

                if (currentCategory == null)
                {
                    context.Categories.Add(category);
                    await context.SaveChangesAsync();
                    currentCategory = category;
                }
           }
           return currentCategory;            
        }

        public async void DeleteCategoryByIdAsync(int id)
        {
            var category = new Category();
            using (var context = new BlogContext())
            {
                category = await context.Categories.FirstOrDefaultAsync(x => x.Id == id);

                if (category != null)
                {
                    context.Categories.Remove(category);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<Category> UpdateCategoryAsync(Category category)
        {
            var currentCategory = new Category();
            using (var context = new BlogContext())
            {
                currentCategory = await context.Categories.FirstOrDefaultAsync(x => x.Id == category.Id);

                if (currentCategory != null)
                {
                    context.Categories.AddOrUpdate(category);
                    await context.SaveChangesAsync();
                    currentCategory = category;
                }
            }
            return currentCategory;
        }
    }
}
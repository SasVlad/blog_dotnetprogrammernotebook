﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int UserId { get; set; }
        public string CommentText { get; set; }
        public DateTime DateRecord { get; set; }
        public Post Post { get; set; }
        public User User { get; set; }
    }
}
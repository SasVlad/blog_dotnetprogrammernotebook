﻿blogApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/allPosts', {
                templateUrl: '/Post/GetAllPosts',
                controller: 'postController'
            })
            .when('/postsByCategory/:categoryId', {
                templateUrl: '/Post/GetAllPosts',
                controller: 'postsByCategoryIdController'
            })
            .when('/newPost/:categoryId', {
                templateUrl: '/Post/CreatePost',
                controller: 'createNewPostController'
            })
            .when('/newCategory', {
                templateUrl: '/Category/CreateCategory',
                controller: 'createCategoryController'
            })
            .when('/newUser', {
                templateUrl: '/User/CreateUser',
                controller: 'createUserController'
            })
            .when('/allCategories', {
                templateUrl: '/Category/GetAllCategories',
                controller: 'categoryController'
            })
            .when('/post/:postid', {
                templateUrl: '/Post/GetPostById',
                controller: 'postByIdController'
            })
            .when('/authorizeUser', {
                templateUrl: '/User/AuthorizeUser',
                controller: 'authorizeUserController'
            })
            .when('/restorePasswordCheckEmail', {
                templateUrl: '/User/RestorePasswordCheckEmailUser',
                controller: 'restorePasswordCheckEmailController'
            })
            .when('/restorePasswordCheckKeyQuestion/:userId', {
                templateUrl: '/User/RestorePasswordCheckKeyQuestionUser',
                controller: 'restorePasswordCheckKeyQuestionController'
            })
            .when('/restorePassword/:userId', {
                templateUrl: '/User/RestorePasswordUser',
                controller: 'restorePasswordController'
            })
            .otherwise({
                redirectTo: '/postsByCategory/1'
            })
    }
]);
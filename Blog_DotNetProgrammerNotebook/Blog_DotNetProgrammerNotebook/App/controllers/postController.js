﻿blogApp.controller('postController', function ($scope, $http) {
    $http({
        method: 'GET',
        url: '/Post/GetAllPostsJson'
    }).then(function (response) {
        $scope.posts = response.data;
    });
    
})
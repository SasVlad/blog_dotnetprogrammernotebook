﻿blogApp.controller('postByIdController', function ($scope,$cookieStore, $http, $routeParams) {
    $scope.user = $cookieStore.get('currentUser');

    $http({
        method: 'GET',
        url: '/Post/GetPostByIdJson',
        params: { postid: $routeParams.postid }
    }).then(function (response) {
        $scope.post = response.data;
        
    });

    $scope._newComment = {
        PostId: $routeParams.postid,
        UserId: 0,
        CommentText: "",
    };
    $scope.CreateNewComment = function () {
        
            $scope._newComment.UserId = $scope.user.Id;
            $http({
                method: 'POST',
                url: '../Comment/CreateComment',
                data: $scope._newComment
            }).then(function () {
                window.location.reload();
            });           
    };
})
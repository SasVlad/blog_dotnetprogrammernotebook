﻿blogApp.controller('authorizeUserController',function ($cookieStore, $scope, $location, $http) {

    $scope._user = {
        Email: "",
        Password:""
    }

    $scope.authorize = function () {
        $http({
            method: 'POST',
            url: '/User/CheckAuthorizeUserJson',
            data: $scope._user
        }).then(function (response) {
            if (response.data.Id === 0) {
                alert("Email or password is wrong");
            }
            if (response.data.Id !== 0) {
                $cookieStore.put('currentUser', response.data);
                window.location.reload();
                $location.path('/postsByCategory/1');
            }

            
        });
    }
    $scope.changePassword = function () {
        $location.path('/restorePasswordCheckEmail');
    }
    $scope.createNewUser = function () {
        $location.path('/newUser');
    }
    
})
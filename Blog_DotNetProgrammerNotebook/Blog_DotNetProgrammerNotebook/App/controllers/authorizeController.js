﻿blogApp.controller('authorizeController', function ($scope, $cookieStore, $location) {

    //console.log("cookie" + $cookieStore.get('currentUser').FirstName);
    $scope._user = $cookieStore.get('currentUser');

    $scope.logIn = function () {
        $location.path('/authorizeUser');
    }
    $scope.logOut = function () {
        $cookieStore.remove('currentUser');
        window.location.reload();
    }
})
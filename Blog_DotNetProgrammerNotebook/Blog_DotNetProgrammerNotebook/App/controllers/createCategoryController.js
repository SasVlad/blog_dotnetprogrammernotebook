﻿blogApp.controller('createCategoryController', function ($scope, $http, $cookieStore, $location) {

    $scope.user = $cookieStore.get('currentUser');
    $scope.newCategory = {
        Name: ""
    }

    $scope.createNewCategory = function () {
        $http({
            method: 'POST',
            url: '../Category/CreateCategory',
            data: $scope.newCategory
        }).then(function (response) {            
            $location.path('/postsByCategory/' + response.data.Id);
        });
    }
});
﻿blogApp.controller('createNewPostController', function ($scope, $http, $cookieStore, $routeParams, $location) {

    $scope.user = $cookieStore.get('currentUser');
    
    $scope._post = {
        Title: "",
        Article: "",
        CategoryId: $routeParams.categoryId,
        AuthorId: 0
    }

    $scope.createNewPost = function () {

        $scope._post.AuthorId = $scope.user.Id;
            $http({
                method: 'POST',
                url: '../Post/CreatePost',
                data: $scope._post
            }).then(function (response) {
                $location.path('/post/' + response.data.Id);
            })
    }
});
﻿blogApp.controller('categoryController', function ($scope, $http, $location) {
$http({
    method: 'GET',
    url: '../Category/GetAllCategories'
    }).then(function (response) {
    $scope.categories = response.data;    
    });
$scope.createCategory = function() {
    $location.path('/newCategory');
}


$scope.GetPostsByCategoryId = function (data) {
    $location.path('/postsByCategory/' + data);
}


})
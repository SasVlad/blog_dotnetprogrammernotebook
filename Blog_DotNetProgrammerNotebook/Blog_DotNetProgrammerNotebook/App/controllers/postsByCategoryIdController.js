﻿blogApp.controller('postsByCategoryIdController', function ($scope, $http, $routeParams, $location) {

    //console.log($routeParams.categoryId);
    $http({
        method: 'GET',
        url: '/Category/GetCategoryById',
        params: { categoryId: $routeParams.categoryId }
    }).then(function(response) {
        $scope.categoryName = response.data.Name;
    });
    $http({
            method: 'GET',
            url: '/Post/GetAllPostsByCategoryIdJson',
            params: { categoryId: $routeParams.categoryId }
        }).then(function (response) {
            $scope.posts = response.data;
            //console.log(posts);
        });
    $scope.CreatePost = function () {
        $location.path('/newPost/' + $routeParams.categoryId);
    }
})
﻿blogApp.controller('restorePasswordController', function ($scope, $http, $routeParams, $location) {
    $scope._user = {};
    $http({
        method: 'GET',
        url: '/User/GetUserById',
        params: { userId: $routeParams.userId }
    }).then(function (response) {
        $scope._user = response.data;
        $scope._user.Password = "";
    });
    $scope.restorePassword = function () {
        $http({
            method: 'Post',
            url: '/User/UpdateUser',
            data: $scope._user
        }).then(function (response) {
            alert("Your password are changed");
            $location.path('/postsByCategory/1');
        });
    }
})
﻿blogApp.controller('restorePasswordCheckKeyQuestionController', function ($scope, $http, $routeParams, $location) {

    $scope.userFromDb = {};

    $scope._userAnswer = {
        Id: $routeParams.userId,
        KeyAnswer:""
    };

    $http({
        method: 'GET',
        url: '/User/GetUserById',
        params: { userId: $routeParams.userId }
    }).then(function (response) {
        $scope.userFromDb = response.data;
    });
    $scope.checkKeyQuestion = function () {
        $http({
            method: 'Post',
            url: '/User/CheckUserKeyAnswerJson',
            data: $scope._userAnswer
        }).then(function (response) {
            if (response.data.Id === 0) {
                alert("This is email not correct answer");
            }
            if (response.data.Id !== 0) {
                $location.path('/restorePassword/' + response.data.Id);
            }
            
        });
    }
})
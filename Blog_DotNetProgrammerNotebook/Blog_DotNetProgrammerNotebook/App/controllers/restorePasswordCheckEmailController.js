﻿blogApp.controller('restorePasswordCheckEmailController', function ($scope, $http, $location) {
    $scope._user = {
        Email:""
    };
    $scope.CheckEmailForRestore = function () {
        $http({
            method: 'Post',
            url: '/User/CheckAuthorizeUserJson',
            data: $scope._user
        }).then(function (response) {
            if (response.data.Id===0) {
                alert("This is email not exist");
            }
            if (response.data.Id !== 0) {
                $location.path('/restorePasswordCheckKeyQuestion/' + response.data.Id);
            }
                
        });
    }
})
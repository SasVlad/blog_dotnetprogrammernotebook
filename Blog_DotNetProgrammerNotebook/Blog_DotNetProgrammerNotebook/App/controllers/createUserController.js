﻿blogApp.controller('createUserController', function ($scope, $http, $location) {
    $scope._user = {
        FirstName:"", 
        LastName:"",
        Email:"",
        Password :"",
        KeyQuestion: "",
        KeyAnswer: ""
    }

    $scope.createNewUser = function () {
        $http({
            method: 'POST',
            url: '/User/CreateUser',
            data: $scope._user
        }).then(function (response) {
            $location.path('/postsByCategory/1');
        });
    }
});
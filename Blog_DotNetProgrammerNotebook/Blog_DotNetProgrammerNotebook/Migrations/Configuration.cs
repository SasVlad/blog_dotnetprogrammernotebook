namespace Blog_DotNetProgrammerNotebook.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Blog_DotNetProgrammerNotebook.EF.BlogContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Blog_DotNetProgrammerNotebook.EF.BlogContext";
        }

        protected override void Seed(Blog_DotNetProgrammerNotebook.EF.BlogContext context)
        {
        }
    }
}

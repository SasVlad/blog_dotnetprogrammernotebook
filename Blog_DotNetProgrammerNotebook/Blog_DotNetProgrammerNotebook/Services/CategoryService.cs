﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Services
{
    public class CategoryService
    {
        CategoryRepository _categoryRepository;
        public CategoryService()
        {
            _categoryRepository = new CategoryRepository();
        }
        public async Task<List<Category>> GetAllCategoriesAsync()
        {
            return await _categoryRepository.GetAllCategoriesAsync() ?? new List<Category>();
        }
        public async Task<Category> GetCategoryByIdAsync(int id)
        {
            if (id!=decimal.Zero)
            {
                return await _categoryRepository.GetCategoryByIdAsync(id);
            }
            return null;
        }

        public async Task<Category> CreateCategoryAsync(Category category)
        {
            if (category!=null && category.DateCreated!=null && !string.IsNullOrEmpty(category.Name))
            {
                return await _categoryRepository.CreateCategoryAsync(category);
            }
            return null;
        }

        public async void DeleteCategoryByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                await _categoryRepository.GetCategoryByIdAsync(id);
            }
        }

        public async Task<Category> UpdateCategoryAsync(Category category)
        {
            if (category != null && category.DateCreated != null && !string.IsNullOrEmpty(category.Name))
            {
                return await _categoryRepository.UpdateCategoryAsync(category);
            }
            return null;
        }
    }
}
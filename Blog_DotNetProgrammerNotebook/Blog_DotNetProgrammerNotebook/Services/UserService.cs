﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Services
{
    public class UserService
    {
        UserRepository _userRepository;
        public UserService()
        {
            _userRepository = new UserRepository();
        }
        public async Task<List<User>> GetAllUsersAsync()
        {
            return await _userRepository.GetAllUsersAsync() ?? new List<User>();
        }
        public async Task<User> GetUserByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                return await _userRepository.GetUserByIdAsync(id);
            }
            return null;
        }
        public async Task<User> CheckAuthorizeUserAsync(string email, string password = "")
        {
            if (!string.IsNullOrEmpty(email))
            {
                return await _userRepository.CheckAuthorizeUserAsync(email, password);
            }
            return null;
        }
        public async Task<User> CheckUserKeyAnswer(string keyAnswer, int userId)
        {
            if (!string.IsNullOrEmpty(keyAnswer)&& userId!=decimal.Zero)
            {
                return await _userRepository.CheckUserKeyAnswer(keyAnswer, userId);
            }
            return null;
        }
        public async Task<User> CreateUserAsync(User user)
        {
            if (user != null 
                && !string.IsNullOrEmpty(user.FirstName)
                && !string.IsNullOrEmpty(user.LastName) 
                && !string.IsNullOrEmpty(user.Email))
            {
                return await _userRepository.CreateUserAsync(user);
            }
            return null;
        }

        public async void DeleteUserByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                await _userRepository.GetUserByIdAsync(id);
            }
        }

        public async Task<User> UpdateUserAsync(User user)
        {
            if (user != null
                && !string.IsNullOrEmpty(user.FirstName)
                && !string.IsNullOrEmpty(user.LastName)
                && !string.IsNullOrEmpty(user.Email))
            {
                return await _userRepository.UpdateUserAsync(user);
            }
            return null;
        }
    }
}
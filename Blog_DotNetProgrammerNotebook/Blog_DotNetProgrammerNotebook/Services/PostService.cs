﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Services
{
    public class PostService
    {
        PostRepository _postRepository;
        UserService _userService;
        public PostService()
        {
            _userService = new UserService();
            _postRepository = new PostRepository();
        }

        public async Task<List<Post>> GetAllPostsAsync()
        {
            return await _postRepository.GetAllPostsAsync();
        }
        public async Task<List<Post>> GetAllPostsByCategoryIdAsync(int categoryId)
        {
            if (categoryId != decimal.Zero)
            {
                var result = await _postRepository.GetAllPostsByCategoryIdAsync(categoryId);
                return result;
            }
            return new List<Post>();
        }
        public async Task<List<Post>> GetAllPostsByAuthorIdAsync(int authorId)
        {
            if (authorId != decimal.Zero)
            {
                return await _postRepository.GetAllPostsByAuthorIdAsync(authorId);
            }
            return new List<Post>();
        }
        public async Task<Post> GetPostByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                return await _postRepository.GetPostByIdAsync(id);
            }
            return null;
        }

        public async Task<Post> CreatePostAsync(Post post)
        {
            if (post != null 
                && !string.IsNullOrEmpty(post.Title)
                && !string.IsNullOrEmpty(post.Article) 
                && post.DatePublished != null
                && post.AuthorId != decimal.Zero
                && post.CategoryId != decimal.Zero)
            {
                return await _postRepository.CreatePostAsync(post);
            }
            return null;
        }

        public async void DeletePostByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                await _postRepository.GetPostByIdAsync(id);
            }
        }

        public async Task<Post> UpdatePostAsync(Post post)
        {
            if (post != null
                && !string.IsNullOrEmpty(post.Title)
                && !string.IsNullOrEmpty(post.Article)
                && post.DatePublished != null
                && post.AuthorId != decimal.Zero
                && post.CategoryId != decimal.Zero)
            {
                return await _postRepository.UpdatePostAsync(post);
            }
            return null;
        }
    }
}
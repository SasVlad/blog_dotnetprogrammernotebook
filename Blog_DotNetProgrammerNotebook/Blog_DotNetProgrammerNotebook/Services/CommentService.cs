﻿using Blog_DotNetProgrammerNotebook.Models;
using Blog_DotNetProgrammerNotebook.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.Services
{
    public class CommentService
    {
        CommentRepository _commentRepository;
        UserService _userService;
        public CommentService()
        {
            _commentRepository = new CommentRepository();
            _userService = new UserService();
        }
        public async Task<List<Comment>> GetAllCommentsAsync()
        {                       
            return await _commentRepository.GetAllCommentsAsync() ?? new List<Comment>();
        }
        public async Task<List<Comment>> GetAllCommentsByPostIdAsync(int postid)
        {
            if (postid != decimal.Zero)
            {
                return await _commentRepository.GetAllCommentsByPostIdAsync(postid);
            }
            return new List<Comment>();
        }
        //??
        public async Task<Comment> GetCommentByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                return await _commentRepository.GetCommentByIdAsync(id);
            }
            return null;
        }

        public async Task<Comment> CreateCommentAsync(Comment comment)
        {
            if (comment != null 
                && !string.IsNullOrEmpty(comment.CommentText) 
                && comment.DateRecord!=null 
                && comment.PostId!=decimal.Zero
                && comment.UserId!=decimal.Zero)
            {
                return await _commentRepository.CreateCommentAsync(comment);
            }
            return null;
        }

        public async void DeleteCommentByIdAsync(int id)
        {
            if (id != decimal.Zero)
            {
                await _commentRepository.GetCommentByIdAsync(id);
            }
        }

        public async Task<Comment> UpdateCommentAsync(Comment comment)
        {
            if (comment != null 
                && !string.IsNullOrEmpty(comment.CommentText)
                && comment.DateRecord != null
                && comment.PostId != decimal.Zero
                && comment.UserId != decimal.Zero)
            {
                return await _commentRepository.UpdateCommentAsync(comment);
            }
            return null;
        }
    }
}
﻿using Blog_DotNetProgrammerNotebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_DotNetProgrammerNotebook.EF
{
    public class BlogContext:DbContext
    {
        public BlogContext() : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
           
            Database.SetInitializer<BlogContext>(new BlogDatabaseInitialize());
        }
            public DbSet<Post> Posts { get; set; }
            public DbSet<Category> Categories { get; set; }
            public DbSet<Comment> Comments { get; set; }
            public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>().Ignore(e => e.Post);
        }
    }
}